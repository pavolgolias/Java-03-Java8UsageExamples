package java8.examples;

//source: http://www.mkyong.com/java8/java-8-streams-filter-examples/

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsAndFilterExamples {
    public static void filterAndCollectExample() {
        List<String> lines = Arrays.asList("AAA", "BBB", "CCC");

        List<String> result = lines.stream().
                filter(item -> !item.equals("CCC"))
                .collect(Collectors.toList());

        result.forEach(System.out::println);
    }

    public static void filterAndCollectExample2() {
        List<Item> items = Arrays.asList(new Item("AAA"), new Item("BBB"), new Item("CCC"));

        Item result = items.stream()                                // Convert to steam
                //.filter(item -> item.getName().equals("AAAA"))
                .filter(item -> item.getName().equals("AAA"))       // We want "AAA" only
                .findAny()                                          // If 'findAny' then return found
                .orElse(null);                                      // If not found, return null

        System.out.println("Found item: " + result);
    }

    public static void filterAndCollectExample3() {
        List<Item> items = Arrays
                .asList(new Item("AAA", 1), new Item("BBB", 2), new Item("CCC", 3), new Item("DDD", 4));

        Item result = items.stream()
                .filter(x -> x.getName().equals("CCC") && x.getNumber() > 1)
                .findAny().orElse(null);

        System.out.println("Found item: " + result);

        // or another possible solution
        result = items.stream()
                .filter(x -> {
                    if (x.getName().equals("CCC") && x.getNumber() > 1) {
                        return true;
                    }
                    return false;
                })
                .findAny().orElse(null);

        System.out.println("Found item: " + result);
    }

    public static void filterAndCollectExample4() {
        List<Item> items = Arrays.asList(new Item("AAA", 1), new Item("BBB", 2), new Item("CCC", 3));

        int resultNumber = items.stream()
                .filter(x -> x.getName().equals("BBB"))
                .map(Item::getNumber)                   // convert stream to String
                .findAny()
                .orElse(-1);

        System.out.println("Found number of item: " + resultNumber);
    }

    private static class Item {
        private String name;
        private int number;

        Item(String name) {
            this.name = name;
        }

        Item(String name, int number) {
            this.name = name;
            this.number = number;
        }

        String getName() {
            return name;
        }

        int getNumber() {
            return number;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "name='" + name + '\'' +
                    ", number=" + number +
                    '}';
        }
    }
}
