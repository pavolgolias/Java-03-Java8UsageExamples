package java8.examples;

// source: http://www.mkyong.com/java8/java-8-filter-a-map-examples/

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class FilterMapExamples {
    public static void mapFilteringToString() {
        Map<Integer, String> items = new HashMap<>();
        items.put(1, "item1");
        items.put(2, "item2");
        items.put(3, "item3");
        items.put(4, "item4");

        // Map -> Stream -> Filter -> String
        String result = items.entrySet().stream()
                .filter(item -> "item2".equals(item.getValue()))
                .map(Map.Entry::getValue)
                .collect(Collectors.joining());

        System.out.println("Filtering map to one value with Java 8: " + result);
    }

    public static void mapFilteringToMap() {
        Map<Integer, String> items = new HashMap<>();
        items.put(1, "item1");
        items.put(2, "item2");
        items.put(3, "item3");
        items.put(4, "item4");

        // Map -> Stream -> Filter -> Map
        Map<Integer, String> resultMap = items.entrySet().stream()
                .filter(item -> (item.getKey() == 3 || item.getKey() == 1))
                //.collect(Collectors.toMap(item -> item.getKey(), item -> item.getValue()));
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        System.out.println("Filtering map to another map with Java 8: " + resultMap);
    }

}
