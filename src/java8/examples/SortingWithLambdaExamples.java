package java8.examples;

//source: http://www.mkyong.com/java8/java-8-lambda-comparator-example/

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortingWithLambdaExamples {
    public static void sortingWithLambdaExample() {
        List<Developer> developers = new ArrayList<>();

        developers.add(new Developer("me", new BigDecimal("70000"), 33));
        developers.add(new Developer("you", new BigDecimal("80000"), 20));
        developers.add(new Developer("we", new BigDecimal("100000"), 10));
        developers.add(new Developer("all of us", new BigDecimal("170000"), 55));

        System.out.println("Before sorting:");
        developers.forEach(System.out::println);

        /* sorting by age */
        //developers.sort((Developer o1, Developer o2) -> o1.getAge() - o2.getAge());

        // or this - parameter type is optional
        developers.sort((o1, o2) -> o1.getAge() - o2.getAge());

        System.out.println("\nAfter sorting:");
        developers.forEach(System.out::println);

        /* sorting by name */
        //developers.sort((Developer o1, Developer o2) -> o1.getName().compareTo(o2.getName()));
        developers.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));

        System.out.println("\nAfter sorting:");
        developers.forEach(System.out::println);

        /* sorting by salary */
        developers.sort((Developer o1, Developer o2) -> o1.getSalary().compareTo(o2.getSalary()));
        //developers.sort((o1, o2)->o1.getSalary().compareTo(o2.getSalary()));

        System.out.println("\nAfter sorting:");
        developers.forEach(System.out::println);

        /* reversed sorting by salary */
        Comparator<Developer> salaryComparator = (o1, o2) -> o1.getSalary().compareTo(o2.getSalary());
        developers.sort(salaryComparator.reversed());

        System.out.println("\nAfter sorting:");
        developers.forEach(System.out::println);
    }

    private static class Developer {
        private String name;
        private BigDecimal salary;
        private int age;

        Developer(String name, BigDecimal salary, int age) {
            this.name = name;
            this.salary = salary;
            this.age = age;
        }

        String getName() {
            return name;
        }

        BigDecimal getSalary() {
            return salary;
        }

        int getAge() {
            return age;
        }

        @Override
        public String toString() {
            return "Developer{" +
                    "name='" + name + '\'' +
                    ", salary=" + salary +
                    ", age=" + age +
                    '}';
        }
    }
}
