package java8.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// source: http://www.mkyong.com/java8/java-8-foreach-examples/

public class ForEachExamples {
    public static void mapIterationExample() {
        Map<Integer, String> items = new HashMap<>();
        items.put(1, "item1");
        items.put(2, "item2");
        items.put(3, "item3");
        items.put(4, "item4");

        items.forEach((k, v) -> System.out.println("Item: " + k + ", value: " + v));

        System.out.println();
        items.forEach((k, v) -> {
            System.out.println("Item: " + k + ", value: " + v);

            if (k == 2) {
                System.out.println("This is second item.");
            }
        });
    }

    public static void listIterationExample() {
        List<Integer> items = new ArrayList<>();
        items.add(1);
        items.add(2);
        items.add(3);
        items.add(4);

        items.forEach(item -> System.out.println("Item: " + item));

        System.out.println();
        items.forEach(item -> {
            if (item == 1) {
                System.out.println("Item 1 found.");
            }
        });

        System.out.println();
        items.forEach(System.out::println);

        System.out.println();
        items.stream().filter(item -> (item == 1 || item == 2)).forEach(System.out::println);
    }
}
