package java8.examples;

//source: http://javarevisited.blogspot.sk/2014/02/10-example-of-lambda-expressions-in-java8.html

public class ThreadExamples {
    public static void startThreadExample() {
        new Thread(() -> System.out.println("This is thread")).start();

        new Thread(() -> {
            System.out.println("Multiple");
            System.out.println("commands");
            System.out.println("example.");
        }).start();
    }
}
