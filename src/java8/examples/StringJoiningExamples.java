package java8.examples;

// source: http://www.mkyong.com/java8/java-8-stringjoiner-example/

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class StringJoiningExamples {
    public static void stringJoinerUsage() {
        StringJoiner joiner = new StringJoiner(",");
        joiner.add("aa");
        joiner.add("bb");
        joiner.add("cc");
        System.out.println("String joined by StringJoiner: " + joiner.toString());

        joiner = new StringJoiner("/", "prefix-", "-suffix");
        joiner.add("11");
        joiner.add("22");
        joiner.add("33");
        System.out.println("String joined by StringJoiner: " + joiner.toString());
    }

    public static void stringJoinMethodUsage() {
        String result = String.join("-", "2016", "03", "30");
        System.out.println("String joined by String.join: " + result);

        List<String> list = Arrays.asList("java", "python", "nodeJs", "ruby");
        result = String.join(", ", list);
        System.out.println("String joined by String.join: " + result);
    }

    public static void collectorsJoiningUsage() {
        List<String> list = Arrays.asList("java", "python", "nodeJs", "ruby");
        String result = list.stream()/*.map(x -> x)*/.collect(Collectors.joining(" | "));
        System.out.println("String joined by Collectors.joining: " + result);
    }
}
