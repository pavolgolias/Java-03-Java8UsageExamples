package java8.examples;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// source: http://www.mkyong.com/java8/java-8-stream-read-a-file-line-by-line/

public class ReadFileExamples {
    public static void fileReadExample1() {
        List<String> lines = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(Paths.get("demoFile.txt"))) {
            lines = br.lines().collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        lines.forEach(System.out::println);
    }

    public static void fileReadExample2() {
        List<String> lines = new ArrayList<>();

        try {
            lines = Files.lines(Paths.get("demoFile.txt"))
                    .filter(line -> !line.equals("line 2"))
                    .map(String::toUpperCase)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        lines.forEach(System.out::println);
    }
}
