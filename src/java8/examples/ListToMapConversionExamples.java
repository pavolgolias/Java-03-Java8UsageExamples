package java8.examples;

// source: http://www.mkyong.com/java8/java-8-convert-list-to-map/

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListToMapConversionExamples {
    public static void convert() {
        List<Hosting> items = new ArrayList<>();
        items.add(new Hosting(1, "liquidweb.com", new Date()));
        items.add(new Hosting(2, "linode.com", new Date()));
        items.add(new Hosting(3, "digitalocean.com", new Date()));

        Map<Integer, String> resultMap = items.stream()
                //.collect(Collectors.toMap(x -> x.getId(), x-> x.getName()));
                .collect(Collectors.toMap(Hosting::getId, Hosting::getName));

        resultMap.entrySet()
                .forEach(mapItem -> System.out.println("Item: " + mapItem.getKey() + ", value: " + mapItem.getValue()));
    }

    private static class Hosting {
        private int id;
        private String name;
        private Date createdDate;

        public Hosting(int id, String name, Date createdDate) {
            this.id = id;
            this.name = name;
            this.createdDate = createdDate;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Date getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(Date createdDate) {
            this.createdDate = createdDate;
        }
    }
}
