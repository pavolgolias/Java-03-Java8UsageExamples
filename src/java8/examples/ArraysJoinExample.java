package java8.examples;

// source: http://www.mkyong.com/java/java-how-to-join-arrays/

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ArraysJoinExample {
    public static void joinArrays() {
        String[] s1 = new String[]{"a", "b"};
        String[] s2 = new String[]{"c", "d"};
        String[] s3 = new String[]{"e", "f"};

        // joining objects type array
        String[] stringResult = Stream.of(s1, s2, s3).flatMap(Stream::of).toArray(String[]::new);
        System.out.println("Joined String Array: " + Arrays.toString(stringResult));

        // joining primitive arrays
        int[] int1 = new int[]{1, 2, 3};
        int[] int2 = new int[]{4, 5, 6};
        int[] int3 = new int[]{7, 8, 9};

        int[] intResult = IntStream
                .concat(Arrays.stream(int1), IntStream.concat(Arrays.stream(int2), Arrays.stream(int3))).toArray();
        System.out.println("Joined primitive type arrays (int): " + Arrays.toString(intResult));
    }
}
