package java8;

import java8.examples.*;

public class Main {
    public static void main(String[] args) {
        //ForEachExamples.mapIterationExample();
        //ForEachExamples.listIterationExample();

        //FilterMapExamples.mapFilteringToString();
        //FilterMapExamples.mapFilteringToMap();

        //ReadFileExamples.fileReadExample1();
        //ReadFileExamples.fileReadExample2();

        //ListToMapConversionExamples.convert();

        //ArraysJoinExample.joinArrays();

        //StringJoiningExamples.stringJoinerUsage();
        //StringJoiningExamples.stringJoinMethodUsage();
        //StringJoiningExamples.collectorsJoiningUsage();

        //StreamsAndFilterExamples.filterAndCollectExample();
        //StreamsAndFilterExamples.filterAndCollectExample2();
        //StreamsAndFilterExamples.filterAndCollectExample3();
        //StreamsAndFilterExamples.filterAndCollectExample4();

        //SortingWithLambdaExamples.sortingWithLambdaExample();

        ThreadExamples.startThreadExample();
    }
}
