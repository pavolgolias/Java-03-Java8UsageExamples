﻿## Examples of Java 8 features usage

 - Arrays joining
 - Filter and Map
 - ForEach
 - List to Map conversion
 - File read
 - Sorting with Lambdas
 - Streams and filter
 - String joining
 - Threads
